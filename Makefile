
SOURCES=$(wildcard src/*.svg)
TEMPLATES=$(wildcard src/*.hbs)
DOCASSETS=dist/onion-bg.svg dist/bootstrap.css
PNG16=$(patsubst src/%,dist/16x16/%,$(patsubst %.svg,%.png,$(SOURCES)))
PNG32=$(patsubst src/%,dist/32x32/%,$(patsubst %.svg,%.png,$(SOURCES)))
PNG64=$(patsubst src/%,dist/64x64/%,$(patsubst %.svg,%.png,$(SOURCES)))
PNG128=$(patsubst src/%,dist/128x128/%,$(patsubst %.svg,%.png,$(SOURCES)))

all: $(SOURCES) $(TEMPLATES) $(DOCASSETS) $(PNG16) $(PNG32) $(PNG64) $(PNG128)
	icon-font-generator \
		-n tor-icons \
		--htmltp templates/html.hbs \
		--csstp templates/css.hbs \
		-p ti \
		-t .ti \
		--normalize true \
		--center true src/*.svg \
		-o dist
	-( cd dist ; ln -s tor-icons.html index.html )

dist/16x16/%: dist dist/16x16
	inkscape -z -e $@ -h 16 src/$(notdir $(basename $@)).svg

dist/32x32/%: dist dist/32x32
	inkscape -z -e $@ -h 32 src/$(notdir $(basename $@)).svg

dist/64x64/%: dist dist/64x64
	inkscape -z -e $@ -h 64 src/$(notdir $(basename $@)).svg

dist/128x128/%: dist dist/128x128
	inkscape -z -e $@ -h 128 src/$(notdir $(basename $@)).svg

dist:
	mkdir dist

dist/16x16:
	mkdir dist/16x16

dist/32x32:
	mkdir dist/32x32

dist/64x64:
	mkdir dist/64x64

dist/128x128:
	mkdir dist/128x128

dist/onion-bg.svg: dist
	cp templates/onion-bg.svg dist/

dist/bootstrap.css: dist
	cp templates/bootstrap.css dist/

zip:
	zip -r tor-icons.zip dist/
